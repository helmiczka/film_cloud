# Copyright (c) 2018 Samuel Mudrík
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import sys
import requests
from bs4 import BeautifulSoup
from collections import Counter
import multiprocessing

topFilmRating = "*"*5

# Needed because of the Windows command line encoding
def unicodePrint(string, file=None):
    print(string.encode('utf8').decode(file.encoding), file=file)

class TasteSummary:
    id = ""
    topFilms = set()
    topFilmsCount = 0
    genres = []
    directors = []
    
    def __init__(self, id):
        self.id = id
    
    def getTopGenres(self):
        return Counter(self.genres).most_common(10)
    
    def getTopDirectors(self):
        return Counter(self.directors).most_common(10)

class CSFD_Cloud:
    outputFile = sys.stdout
    
    def print(string):
        unicodePrint(string, CSFD_Cloud.outputFile)
    
    @staticmethod
    def isTopFilm(film):
        rating = film.find("img")
        if not rating:
            return False
        else:
            return topFilmRating in rating.get("alt")
    
    def setOutputFile(self, file):
        outputFile = file
    
    @staticmethod
    def getHTMLPage(url):
        source = requests.get(url)
        return BeautifulSoup(source.text, "html.parser")
    
    @staticmethod
    def getTable(page):
        return page.find("table", { "class" : "ui-table-list" })
    
    @staticmethod
    def fetchTopFilms(userId):
        topFilms = set()
        nextPageNeeded = True
        i = 1

        while nextPageNeeded:
            url = "https://www.csfd.cz/uzivatel/" + userId + "/hodnoceni/podle-rating/strana-" + str(i) + "/"
            i += 1
            table = CSFD_Cloud.getTable(CSFD_Cloud.getHTMLPage(url))
            if table:
                for film in table.tbody.find_all("tr"):
                    filmName = film.td.a.text
                    filmURL = film.td.a['href'] 
                    if CSFD_Cloud.isTopFilm(film):
                        topFilms.add((filmName, filmURL))
                    else:
                        nextPageNeeded = False
            else:
                break

        return topFilms

    @staticmethod
    def fetchFilmInfo(film):
        page = CSFD_Cloud.getHTMLPage("https://www.csfd.cz"+film[1])
        genres = page.find("p", {"class": "genre"}).text
        directors = page.find("span", {"itemprop": "director"})
        if directors:
            directors = directors.a.text
        else:
            directors = ""
        return (genres.split(" / "), directors.split(", "))
    
    def generate(self, id):
        summary = TasteSummary(id)
        summary.topFilms = CSFD_Cloud.fetchTopFilms(id)
        
        pool = multiprocessing.Pool(processes=4)
        pool_outputs = pool.map(CSFD_Cloud.fetchFilmInfo, summary.topFilms)
        pool.close()
        pool.join()
        filmInfos = pool_outputs
        
        for info in filmInfos:
            summary.genres.extend(info[0])
            summary.directors.extend(info[1])
        
        return summary
    
    def printResults(self, summary):
        CSFD_Cloud.print("The user " + id + " has " + str(len(summary.topFilms)) + " top-rated films")
        CSFD_Cloud.print("His/her top-rated genres are: ")

        for genre in summary.getTopGenres():
            CSFD_Cloud.print(genre[0])
            CSFD_Cloud.print(str(genre[1]))

        for director in summary.getTopDirectors():
            CSFD_Cloud.print(director[0])
            CSFD_Cloud.print(str(director[1]))
    

if __name__ == "__main__":
    id = sys.argv[1]
    Cloud = CSFD_Cloud()
    outputFile = open(sys.argv[2], encoding='utf-8', mode='w') if len(sys.argv) > 2 else sys.stdout
    Cloud.setOutputFile(outputFile)
    Cloud.printResults(Cloud.generate(id))
