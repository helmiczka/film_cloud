# Copyright (c) 2018 Samuel Mudrík
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from flask import Flask, render_template, request
from generate_cloud import CSFD_Cloud

application = Flask(__name__)

@application.route("/", methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        id = request.form['id']
        Cloud = CSFD_Cloud()
        summary = Cloud.generate(id)
        return render_template('page.html', summary=summary)
    return render_template('page.html')

if __name__ == "__main__":
    application.run()
